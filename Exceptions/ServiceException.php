<?php

namespace Exceptions;

use RuntimeException;
use Throwable;

class ServiceException extends RuntimeException
{
    public function __construct($message = 'An unexpected error occurred.', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}