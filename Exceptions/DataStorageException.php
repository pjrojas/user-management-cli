<?php

namespace Exceptions;

use Exception;
use Throwable;

class DataStorageException extends Exception
{
    public function __construct(string $message = "An error occurred while accessing the data storage.", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
