<?php

namespace Repositories;

use Exceptions\DataStorageException;
use Models\User;

interface UserRepositoryInterface
{
    /**
     * Add a new user to the database with the given name.
     *
     * @param string $name The name of the user to add.
     * @return User The created User object.
     * @throws DataStorageException If there is an error accessing the data storage.
     */
    public function addUser(string $name) : User;

    /**
     * Retrieve a user by ID from the database.
     *
     * @param string $id The ID of the user to retrieve.
     * @return User|null The User object if found, or null if not found.
     * @throws DataStorageException If there is an error retrieving the user.
     */
    public function getUserById(string $id): ?User;
}