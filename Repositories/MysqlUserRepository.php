<?php

namespace Repositories;

use Exceptions\DataStorageException;
use Models\User;
use PDO;
use PDOException;
use Ramsey\Uuid\Uuid;
use Throwable;

class MysqlUserRepository implements UserRepositoryInterface
{
    private PDO $pdo;

    /**
     * @param string $databaseHost
     * @param string $databaseName
     * @param string $databaseUser
     * @param string $databasePassword
     * @throws DataStorageException
     */
    public function __construct(
        string $databaseHost,
        string $databaseName,
        string $databaseUser,
        string $databasePassword
    ) {
        // Try to create the MySQL connection using PDO
        try {
            $this->pdo = new PDO("mysql:host=$databaseHost;dbname=$databaseName", $databaseUser, $databasePassword);

            // Configure PDO to throw exceptions in case of errors
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new DataStorageException("Failed to establish database connection: " . $e->getMessage());
        }
    }

    /**
     * Add a new user to the database with the given name.
     *
     * @param string $name The name of the user to add.
     * @return User The created User object.
     * @throws DataStorageException If there is an error accessing the data storage.
     */
    public function addUser(string $name) : User
    {
        $uuid = Uuid::uuid4()->toString();

        try {
            $stmt = $this->pdo->prepare("INSERT INTO users (id, name) VALUES (:id, :name)");
            $stmt->bindParam(':id', $uuid);
            $stmt->bindParam(':name', $name);
            $stmt->execute();
        } catch (Throwable $e) {
            throw new DataStorageException("Error adding user: " . $e->getMessage(), previous: $e);
        }

        return new User(
            id: $uuid,
            name: $name
        );
    }

    /**
     * Retrieve a user by ID from the database.
     *
     * @param string $id The ID of the user to retrieve.
     * @return User|null The User object if found, or null if not found.
     * @throws DataStorageException If there is an error retrieving the user.
     */
    public function getUserById(string $id): ?User
    {
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM users WHERE id = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!$row) {
                return null;
            }

            return new User(
                id: $row['id'],
                name: $row['name']
            );
        } catch (PDOException $e) {
            throw new DataStorageException("Error retrieving user with ID $id: " . $e->getMessage());
        }
    }
}