PHP Test Assignment
---

This PHP CLI script receives the following options: 
* `action` (required): should contain one of the following values `create` or `find`. 
* `name` (conditionally required): required as second option if the `action` option is `create`, if the name consists of more than one word, it should be enclosed in quotes.
* `id` (conditionally required): required as second option if the `action` option is `find`, it should be in the UUID format.

***

### How to Run

Go to the project folder
```
cd existing_folder
```
Copy the contents of the `.env.example` file into a new file called `.env` and update the values of the variables.
```bash
cp .env.example .env
```

To run, execute the following in the terminal:

```bash
php app.php --action [create/find] [--name] [--id]
```
***

### Error Handling
This command will return the following error messages if:
* The 'action' parameter is not provided. 
* The value of the 'action' parameter is not 'create' or 'find'. 
* The required second parameter was not provided.

*** 

### Example

* To create a new user with the name "John", run the following command:

```bash
php app.php --action create --name=John
```

* To find a user with the ID "1234", run the following command:

```bash
php app.php --action find --id=c4a760a8-dbcf-4e14-9fde-214b1c4368f4
```
***

### Testing

```bash
composer test
```
