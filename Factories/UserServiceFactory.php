<?php

namespace Factories;

use Exceptions\DataStorageException;
use Exceptions\ServiceException;
use Repositories\MysqlUserRepository;
use Services\UserService;
use Services\UserServiceInterface;

class UserServiceFactory
{

    public static function create(): UserServiceInterface
    {
        // Get the connection credentials from the environment variables
        $databaseHost     = $_ENV['DB_HOST'];
        $databaseName     = $_ENV['DB_NAME'];
        $databaseUser     = $_ENV['DB_USER'];
        $databasePassword = $_ENV['DB_PASS'];

        // Validate the environment variables
        if (!$databaseHost || !$databaseName || !$databaseUser || !$databasePassword) {
            throw new ServiceException("Database connection credentials are missing or invalid.");
        }

        try {
            $repository = new MysqlUserRepository(
                databaseHost: $databaseHost,
                databaseName: $databaseName,
                databaseUser: $databaseUser,
                databasePassword: $databasePassword
            );
        } catch (DataStorageException $e) {
            throw new ServiceException(previous: $e);
        }

        return new UserService($repository);
    }
}
