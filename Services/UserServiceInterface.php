<?php

namespace Services;

use Exceptions\DataStorageException;
use Exceptions\ServiceException;
use Models\User;
use Throwable;

interface UserServiceInterface
{
    /**
     * Add a new user.
     *
     * @param string $name The name of the user.
     * @return User The created User object.
     *
     * @throws ServiceException If there is an error adding the user.
     */
    public function addUser(string $name): User;

    /**
     * Retrieve a user by ID from the database.
     *
     * @param string $id The ID of the user to retrieve.
     * @return User|null The User object if found, or null if not found.
     * @throws ServiceException If there is an error retrieving the user.
     */
    public function getUserById(string $id): ?User;
}