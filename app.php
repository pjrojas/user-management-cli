<?php

use App\UserManagementCLI;
use Dotenv\Dotenv;
use Exceptions\ServiceException;
use Factories\UserServiceFactory;


require_once __DIR__.'/vendor/autoload.php';

/**
 * Command line interface for managing users.
 *
 * Usage:
 * php app.php --action=create --name=John                             // Creates a new user named John
 * php app.php --action=find --id=c4a760a8-dbcf-4e14-9fde-214b1c4368f4 // Finds the user with ID c4a760a8-dbcf-4e14-9fde-214b1c4368f4
 */

// Load environment variables from the .env file
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

try {
    $userService = UserServiceFactory::create();
    $options = getopt("", ["action:", "name::", "id::"]);
    $app = new UserManagementCLI($userService);
    echo $app->run($options);
} catch (ServiceException|InvalidArgumentException $e) {
    echo $e->getMessage();
} catch (Throwable $e) {
    echo $e->getMessage();
    echo 'An unexpected error occurred.';
}

