<?php

namespace tests;

use App\UserManagementCLI;
use InvalidArgumentException;
use Models\User;
use PHPUnit\Framework\TestCase;
use Services\UserServiceInterface;

class UserManagementCLITest extends TestCase
{
    private UserServiceInterface $userServiceMock;
    private UserManagementCLI $userManagementCLI;

    public function setUp(): void
    {
        $this->userServiceMock = $this->getMockBuilder(UserServiceInterface::class)
            ->getMock();

        $this->userManagementCLI = new UserManagementCLI($this->userServiceMock);
    }

    public function testCreateNewUser()
    {
        $name = 'John';
        $userId = 'c4a760a8-dbcf-4e14-9fde-214b1c4368f4';

        $options = [
            'action' => 'create',
            'name' => 'John'
        ];

        $userMock = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userMock->method('getId')->willReturn($userId);
        $userMock->method('getName')->willReturn($name);
        $this->userServiceMock->expects($this->once())
            ->method('addUser')
            ->with($name)
            ->willReturn($userMock);

        $expectedOutput = "User created with ID $userId and name '$name'.\n";

        $this->assertEquals($expectedOutput, $this->userManagementCLI->run($options));
    }

    public function testFindUser()
    {
        $userId = 'c4a760a8-dbcf-4e14-9fde-214b1c4368f4';
        $name = 'John';

        $options = [
            'action' => 'find',
            'id' => 'c4a760a8-dbcf-4e14-9fde-214b1c4368f4'
        ];

        $userMock = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userMock->method('getId')->willReturn($userId);
        $userMock->method('getName')->willReturn($name);
        $this->userServiceMock->expects($this->once())
            ->method('getUserById')
            ->with($userId)
            ->willReturn($userMock);

        $expectedOutput = "User found with ID $userId and name '$name'.\n";
        $this->assertEquals($expectedOutput, $this->userManagementCLI->run($options));
    }

    public function testCreateNewUserInvalidAction()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("The value of the 'action' parameter must be 'create' or 'find'.");
        $this->userManagementCLI->run(['action' => 'invalid', 'name' => 'John']);
    }

    public function testFindUserInvalidId()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("The second parameter is required for action 'find'.");
        $this->userManagementCLI->run(['action' => 'find']);
    }
}
