<?php

namespace App;

use Exceptions\ServiceException;
use InvalidArgumentException;
use Services\UserServiceInterface;

class UserManagementCLI
{
    /**
     * User service instance.
     *
     * @var UserServiceInterface
     */
    private UserServiceInterface $userService;

    /**
     * UserCLI constructor.
     *
     * @param UserServiceInterface $userService User service instance.
     */
    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Runs the command line interface.
     *
     * @return string The output of the command line interface.
     *
     * @throws InvalidArgumentException If the provided action or arguments are invalid.
     * @throws ServiceException If there's an error accessing or processing data in the service layer.
     */

    /**
     * @param array $options The command line input
     * @return string
     */
    public function run(array $options): string
    {
        // Verify if the action parameter was provided
        if (!isset($options['action'])) {
            throw new InvalidArgumentException("The 'action' parameter is required.");
        }

        // Verify if the value of the action parameter is valid
        $action = $options['action'];
        if ($action !== 'create' && $action !== 'find') {
            throw new InvalidArgumentException("The value of the 'action' parameter must be 'create' or 'find'.");
        }

        // Verify if the required second parameter was provided
        if (($action === 'create' && !isset($options['name'])) || ($action === 'find' && !isset($options['id']))) {
            throw new InvalidArgumentException("The second parameter is required for action '$action'.");
        }

        return match ($action) {
            'create' => $this->createNewUser($options['name']),
            'find'   => $this->findUser($options['id'])
        };
    }

    /**
     * Creates a new user with the given name and returns a success message.
     *
     * @param string $name The name of the new user.
     * @return string The success message indicating the ID and name of the created user.
     */
    private function createNewUser(string $name): string
    {
        $user = $this->userService->addUser($name);
        return "User created with ID " . $user->getId() . " and name '" . $user->getName() . "'.\n";
    }

    /**
     * Finds a user with the given ID and returns a success message.
     *
     * @param string $id The ID of the user to find.
     * @return string The success message indicating the ID and name of the found user.
     */
    private function findUser(string $id): string
    {
        $user = $this->userService->getUserById($id);
        return "User found with ID " . $user->getId() . " and name '" . $user->getName() . "'.\n";
    }
}